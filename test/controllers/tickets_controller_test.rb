require 'test_helper'

class TicketsControllerTest < ActionController::TestCase
  setup do
    @ticket = tickets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tickets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ticket" do
    assert_difference('Ticket.count') do
      post :create, ticket: { act_id: @ticket.act_id, category_id: @ticket.category_id, customer_id: @ticket.customer_id, employee_id: @ticket.employee_id, hours: @ticket.hours, note: @ticket.note, part: @ticket.part, position_id: @ticket.position_id, price: @ticket.price, quantity: @ticket.quantity, rate: @ticket.rate, service_id: @ticket.service_id, status_id: @ticket.status_id, vehicle_id: @ticket.vehicle_id }
    end

    assert_redirected_to ticket_path(assigns(:ticket))
  end

  test "should show ticket" do
    get :show, id: @ticket
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ticket
    assert_response :success
  end

  test "should update ticket" do
    patch :update, id: @ticket, ticket: { act_id: @ticket.act_id, category_id: @ticket.category_id, customer_id: @ticket.customer_id, employee_id: @ticket.employee_id, hours: @ticket.hours, note: @ticket.note, part: @ticket.part, position_id: @ticket.position_id, price: @ticket.price, quantity: @ticket.quantity, rate: @ticket.rate, service_id: @ticket.service_id, status_id: @ticket.status_id, vehicle_id: @ticket.vehicle_id }
    assert_redirected_to ticket_path(assigns(:ticket))
  end

  test "should destroy ticket" do
    assert_difference('Ticket.count', -1) do
      delete :destroy, id: @ticket
    end

    assert_redirected_to tickets_path
  end
end
