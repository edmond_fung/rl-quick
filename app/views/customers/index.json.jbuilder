json.array!(@customers) do |customer|
  json.extract! customer, :id, :cust_first, :cust_last, :cust_address, :cust_city, :cust_state, :cust_phone, :cust_email
  json.url customer_url(customer, format: :json)
end
