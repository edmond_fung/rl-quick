json.array!(@employees) do |employee|
  json.extract! employee, :id, :emp_first, :emp_last, :emp_address, :emp_city, :emp_state, :emp_phone_string, :emp_email
  json.url employee_url(employee, format: :json)
end
