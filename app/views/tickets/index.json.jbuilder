json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :customer_id, :vehicle_id, :category_id, :service_id, :act_id, :position_id, :employee_id, :status_id, :hours, :rate, :part, :quantity, :price, :note
  json.url ticket_url(ticket, format: :json)
end
