json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :vin, :customer_id, :make, :model, :engine, :model, :transmission
  json.url vehicle_url(vehicle, format: :json)
end
