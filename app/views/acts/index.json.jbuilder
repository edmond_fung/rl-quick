json.array!(@acts) do |act|
  json.extract! act, :id, :action_name
  json.url act_url(act, format: :json)
end
