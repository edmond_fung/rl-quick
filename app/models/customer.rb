class Customer < ActiveRecord::Base
  has_many :tickets
  has_many :vehicles

  def cust_full_name
    "#{cust_first} #{cust_last}"
  end
end
