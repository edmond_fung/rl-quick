class Ticket < ActiveRecord::Base
  belongs_to :customer
  belongs_to :act
  belongs_to :vehicle
  belongs_to :category
  belongs_to :service
  belongs_to :position
  belongs_to :employee
  belongs_to :status
end
