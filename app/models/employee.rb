class Employee < ActiveRecord::Base
  has_many :tickets
  def emp_full_name
    "#{emp_first} #{emp_last}"
  end
end
