class Vehicle < ActiveRecord::Base
  has_many :tickets
  def vehicle_info
    "#{vin} #{make} #{model}"
  end
end
