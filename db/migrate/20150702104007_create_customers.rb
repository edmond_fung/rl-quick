class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.text :cust_first
      t.text :cust_last
      t.text :cust_address
      t.text :cust_city
      t.text :cust_state
      t.string :cust_phone
      t.text :cust_email

      t.timestamps null: false
    end
  end
end
