class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :customer_id
      t.integer :vehicle_id
      t.integer :category_id
      t.integer :service_id
      t.integer :act_id
      t.integer :position_id
      t.integer :employee_id
      t.integer :status_id
      t.integer :hours
      t.integer :rate
      t.text :part
      t.integer :quantity
      t.integer :price
      t.integer :note

      t.timestamps null: false
    end
  end
end
