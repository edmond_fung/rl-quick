class CreateActs < ActiveRecord::Migration
  def change
    create_table :acts do |t|
      t.text :action_name

      t.timestamps null: false
    end
  end
end
