class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.integer :vin
      t.integer :customer_id
      t.text :make
      t.text :model
      t.text :engine
      t.text :model
      t.text :transmission

      t.timestamps null: false
    end
  end
end
