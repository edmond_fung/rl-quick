class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.text :emp_first
      t.text :emp_last
      t.text :emp_address
      t.text :emp_city
      t.text :emp_state
      t.string :emp_phone_string
      t.text :emp_email

      t.timestamps null: false
    end
  end
end
