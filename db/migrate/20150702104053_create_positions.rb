class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.text :position_name

      t.timestamps null: false
    end
  end
end
