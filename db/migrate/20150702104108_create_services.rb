class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.integer :category_id
      t.text :service_name

      t.timestamps null: false
    end
  end
end
