# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Act.delete_all
Category.delete_all
Customer.delete_all
Employee.delete_all
Position.delete_all
Service.delete_all
Status.delete_all
Ticket.delete_all
Vehicle.delete_all



Act.create(action_name:"Install")
Act.create(action_name:"Repair")

Category.create(category_name:"Brakes")
Category.create(category_name:"Suspension")

Customer.create(cust_first:"John", cust_last:"Doe", cust_address:"123 abc", cust_city:"Houston", cust_state:"TX", cust_phone:"1234567890", cust_email:"jd@mail.com")
Customer.create(cust_first:"Jane", cust_last:"Moe", cust_address:"321 cba", cust_city:"Houston", cust_state:"TX", cust_phone:"0987654321", cust_email:"jm@mail.com")

Employee.create(emp_first:"Bob", emp_last:"Long", emp_address:"098 xyz", emp_city:"Houston", emp_state:"TX", emp_phone_string:"1234567890", emp_email:"bl@mail.com")
Employee.create(emp_first:"Sam", emp_last:"Tran", emp_address:"899 zyx", emp_city:"Houston", emp_state:"TX", emp_phone_string:"0987654321", emp_email:"st@mail.com")

Position.create(position_name:"Front")
Position.create(position_name:"Rear")

Service.create(category_id:"1", service_name:"Rotors")
Service.create(category_id:"1", service_name:"Break Pads")
Service.create(category_id:"2", service_name:"Springs")
Service.create(category_id:"2", service_name:"Coils")

Status.create(status_name:"Currently working on")
Status.create(status_name:"Waiting on parts")

Ticket.create(customer_id:"1", vehicle_id:"1", category_id:"2", service_id:"3", act_id:"1", position_id:"1", employee_id:"1", status_id:"1", hours:"5", rate:"30", part:"Apexi", price:"1200")
Ticket.create(customer_id:"2", vehicle_id:"2", category_id:"1", service_id:"2", act_id:"2", position_id:"1", employee_id:"2", status_id:"1", hours:"5", rate:"60", part:"Brembro", price:"500")

Vehicle.create(vin:"111111", customer_id:"1", make:"Ford", model:"Mustang", engine:"5.0", transmission:"Manual")
Vehicle.create(vin:"222222", customer_id:"2", make:"Subaru", model:"WRX", engine:"2.0", transmission:"Manual")

