# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150702104916) do

  create_table "acts", force: :cascade do |t|
    t.text     "action_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "categories", force: :cascade do |t|
    t.text     "category_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "customers", force: :cascade do |t|
    t.text     "cust_first"
    t.text     "cust_last"
    t.text     "cust_address"
    t.text     "cust_city"
    t.text     "cust_state"
    t.string   "cust_phone"
    t.text     "cust_email"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "employees", force: :cascade do |t|
    t.text     "emp_first"
    t.text     "emp_last"
    t.text     "emp_address"
    t.text     "emp_city"
    t.text     "emp_state"
    t.string   "emp_phone_string"
    t.text     "emp_email"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "positions", force: :cascade do |t|
    t.text     "position_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "services", force: :cascade do |t|
    t.integer  "category_id"
    t.text     "service_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.text     "status_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "vehicle_id"
    t.integer  "category_id"
    t.integer  "service_id"
    t.integer  "act_id"
    t.integer  "position_id"
    t.integer  "employee_id"
    t.integer  "status_id"
    t.integer  "hours"
    t.integer  "rate"
    t.text     "part"
    t.integer  "quantity"
    t.integer  "price"
    t.integer  "note"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "vehicles", force: :cascade do |t|
    t.integer  "vin"
    t.integer  "customer_id"
    t.text     "make"
    t.text     "model"
    t.text     "engine"
    t.text     "transmission"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

end
